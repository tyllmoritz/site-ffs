#!/bin/bash

set -eu
set -o pipefail

branch=nightly

TARGETS=(
	ath79-generic
	ath79-nand
	ath79-mikrotik
	bcm27xx-bcm2708
	bcm27xx-bcm2709
	ipq40xx-generic
	ipq806x-generic
	lantiq-xrx200
	lantiq-xway
	mediatek-mt7622
	mpc85xx-p1010
	mpc85xx-p1020
	ramips-mt7620
	ramips-mt7621
	ramips-mt76x8
	rockchip-armv8
	sunxi-cortexa7
	x86-generic
	x86-geode
	x86-legacy
	x86-64
)

sitedir=$(readlink -f $(dirname $0))
make -C gluon update GLUON_SITEDIR="$sitedir" V=1

for target in ${TARGETS[@]}; do
	echo Building $target
	make -C gluon GLUON_TARGET=$target GLUON_AUTOUPATER_BRANCH=$branch GLUON_AUTOUPDATER_ENABLED=1 GLUON_SITEDIR="$sitedir" BROKEN=1 V=1 -j`nproc`
done
