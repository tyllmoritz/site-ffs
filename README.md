# Freifunk Stuttgart Gluon

This is the [Gluon configuration](https://gitlab.freifunk-stuttgart.de/firmware/site-ffs) for [Freifunk Stuttgart](https://freifunk-stuttgart.de).

Firmware versions are built in Gitlab.

We maintain our [own Gluon repository](https://gitlab.freifunk-stuttgart.de/firmware/gluon) to make it easy to apply patches to Gluon. 

Gluon is maintained as a [Git submodule](https://git-scm.com/book/en/v2/Git-Tools-Submodules) in the `gluon` subdirectory. 

## Building

Because we maintain Gluon as a submodule, some special steps are required:

- clone this repository with `--recurse-submodules`: ```git clone --recurse-submodules https://gitlab.freifunk-stuttgart.de/firmware/site-ffs.git``` 
  * If you already clonsed this repo, initalize submodules using `git submodule update --init .`
- run `buildall.sh` to build all targets

### Download Cache

We maintain a [cache of all downloaded files during the build](https://gitlab.freifunk-stuttgart.de/firmware/ffs-openwrt-dl-cache).

## Committing

When changing something in Gluon, keep in mind - besides making the changes in the Gluon repository - you also need to make a commit here that updates the `gluon` submodule. You can do so by:
- changing to the `gluon` subdirectory,
- making your changes,
- commit and push them to the Gluon repository,
- going back to the `site-ffs` directory,
- `git add gluon` and then `git commit` to create a commit which updates the Gluon version in the site-ffs repo.

## Automatic Gluon Update

Minor version updates of Gluon are usually very easy to do. Therefore, we have a daily job which checks for a new minor version of Gluon and performs an [automatic update](https://gitlab.freifunk-stuttgart.de/firmware/ffs-pipeline-nightly) if one is available.

## Branches

For each firmware release (vX.Y) we have a seperate branch.

There is also the experimental branch which is our build against Gluon master. Most bugfixes hence need to be applied to the corresponding branch of the next firmware release and the experimental branch.

## Other repositories

We have a [repository with our custom packages](https://gitlab.freifunk-stuttgart.de/firmware/gluon-packages).

There are [several other repos related to the firmware build](https://gitlab.freifunk-stuttgart.de/firmware).
